package com.voida.bizao;

import com.voida.bizao.core.charging.ChargeProcessorImpl;
import com.voida.bizao.core.messaging.MessageProcessorImpl;
import com.voida.bizao.core.messaging.MessageRetryTaskImpl;
import com.voida.bizao.util.Config;
import com.voida.bizao.util.ModuleConfigInitializer;
import com.voida.charging.BillingSystem;
import com.voida.charging.ChargeTimes;
import com.voida.charging.exception.ChargingException;
import com.voida.charging.process.ChargeBulkUpdateTask;
import com.voida.charging.process.ChargingHandler;
import com.voida.charging.process.ChargingTaskHandler;
import com.voida.charging.request.ChargeRequest;
import com.voida.charging.result.ChargeResult;
import com.voida.charging.result.response.ChargeResponse;
import com.voida.charging.result.response.ChargingResponseCode;
import com.voida.db.DBHandler;
import com.voida.http.handler.ParsRequestHandler;
import com.voida.http.server.HttpServer;
import com.voida.messaging.MessagingSystem;
import com.voida.messaging.exception.MessagingException;
import com.voida.messaging.process.*;
import com.voida.messaging.request.MessagePriority;
import com.voida.messaging.request.MessageRequest;
import com.voida.messaging.response.MessageResponse;
import com.voida.messaging.response.MessageResponseCode;
import com.voida.pin.request.PinRequest;
import com.voida.pin.request.VerifyPinRequest;
import com.voida.pin.request.response.MakePinResponse;
import com.voida.pin.request.response.VerifyPinResponse;
import com.voida.subscription.SubscriptionSystem;
import com.voida.subscription.exception.SubscriptionException;
import com.voida.subscription.request.CheckSubscriptionRequest;
import com.voida.subscription.request.MSISDNDetailRequest;
import com.voida.subscription.request.SubscriptionRequest;
import com.voida.subscription.request.UnsubscriptionRequest;
import com.voida.subscription.request.response.MSISDNDetailResponse;
import com.voida.subscription.request.response.SubscriptionResponse;
import com.voida.subscription.result.SubscriptionResult;
import com.voida.subscription.result.SubscriptionStatus;
import com.voida.util.ModuleConfig;
import com.voida.util.ObjectReferences;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:23 PM
 */

public class Bizao implements MessagingSystem, BillingSystem, SubscriptionSystem {
    private static final Logger logger = LogManager.getLogger(Bizao.class);

    private volatile static boolean isInitialised = false;

    public static volatile ModuleConfig moduleConfig = ModuleConfigInitializer.initialize();
    public static volatile ObjectReferences references = new ObjectReferences();
    public static volatile DBHandler dbHandler = new DBHandler(moduleConfig);

    private ChargeProcessorImpl chargeProcessor;

    private HttpServer httpServer;

    private ChargingHandler chargingHandler;
    private ChargingTaskHandler chargingTaskHandler;
    private ChargeBulkUpdateTask chargeBulkUpdateTask;

    private MessageHandler messageHandler;
    private MessageRetryHandler messageRetryHandler;
    private SmsMoBulkInsertTask smsMoBulkInsertTask;
    private SmsMtBulkUpdateTask smsMtBulkUpdateTask;
    private NotificationBulkSaveTask notificationBulkSaveTask;
    private MessagingTaskHandler messagingTaskHandler;

    private ParsRequestHandler parsRequestHandler;

    public static void main(String[] args) {

        Thread httpThread = new Thread(() -> {
            try {
                Bizao bizao = new Bizao();
                bizao.start();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        });
        httpThread.start();
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public Bizao() {
        try {
            if (!isInitialised) {
                logger.info("Initialising Bakcell...");
                logger.info(moduleConfig.toString());
                start();
            } else {
                logger.info("Bakcell already initialised... creating new instance");
                start();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    private void start() {
        try {
            if (!isInitialised) {
                isInitialised = true;

                references.setDbHandler(dbHandler);

                httpServer = new HttpServer(moduleConfig);
                httpServer.startServer();

                smsMoBulkInsertTask = new SmsMoBulkInsertTask(moduleConfig,references);
                smsMoBulkInsertTask.start();
                references.setSmsMoBulkInsertTask(smsMoBulkInsertTask);

                smsMtBulkUpdateTask = new SmsMtBulkUpdateTask(moduleConfig,references);
                smsMtBulkUpdateTask.start();
                references.setSmsMtBulkUpdateTask(smsMtBulkUpdateTask);

                notificationBulkSaveTask = new NotificationBulkSaveTask(moduleConfig,references);
                notificationBulkSaveTask.start();
                references.setNotificationBulkSaveTask(notificationBulkSaveTask);

                messageHandler = new MessageHandler(moduleConfig, references);
                messageHandler.init();
                references.setMessageHandler(messageHandler);

                messageRetryHandler = new MessageRetryHandler(moduleConfig, MessageRetryTaskImpl.class);
                messageRetryHandler.init(Config.MESSAGING_WORKING_HOURS, references.getDbHandler().getRetryPolicies());
                references.setMessageRetryHandler(messageRetryHandler);

                chargingHandler = new ChargingHandler(moduleConfig,references);
                chargingHandler.init();
                references.setChargingHandler(chargingHandler);

                messagingTaskHandler = new MessagingTaskHandler(moduleConfig, references, MessageProcessorImpl.class);
                messagingTaskHandler.start();
                references.setMessageHandler(messageHandler);

                chargingTaskHandler = new ChargingTaskHandler(moduleConfig, references, ChargeProcessorImpl.class);
                chargingTaskHandler.start();
                references.setChargingTaskHandler(chargingTaskHandler);

                chargeBulkUpdateTask = new ChargeBulkUpdateTask(moduleConfig,references);
                chargeBulkUpdateTask.start();
                references.setChargeBulkUpdateTask(chargeBulkUpdateTask);

                chargeProcessor = new ChargeProcessorImpl();
                references.setChargeProcessor(chargeProcessor);

                parsRequestHandler = new ParsRequestHandler(moduleConfig,references);
            }
        } catch (Exception e) {
            try {
                httpServer.stopServer();
            } catch (Exception e1) {
                logger.error(e1.getMessage(), e1);
            }
            isInitialised = false;
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop() {

        try {
            httpServer.stopServer();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        smsMoBulkInsertTask.stop();
        smsMtBulkUpdateTask.stop();
        notificationBulkSaveTask.stop();
        messageRetryHandler.stop();
        messagingTaskHandler.shutdown();
        chargingTaskHandler.shutdown();
        chargeBulkUpdateTask.stop();

        dbHandler = null;
        moduleConfig = null;
        references = null;
        isInitialised = false;
    }

    @Override
    public ChargeResult immediatePurchaseRequest(ChargeRequest chargeRequest) throws ChargingException {
        try {
            return references.getChargeProcessor().processImmediateCharging(chargeRequest);
        } catch (Exception e) {
            throw new ChargingException(e.getMessage(), e);
        }
    }

    @Override
    public ChargeResponse processPurchaseRequest(ChargeRequest chargeRequest) {
        try {
            references.getChargingHandler().submitChargingRequest(chargeRequest);
            return new ChargeResponse(ChargingResponseCode.OK, "Charging request has been accepted.");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return new ChargeResponse(ChargingResponseCode.ERROR, "Charging request has not been accepted.");
    }

    @Override
    public Map<String, ChargeResponse> processBulkPurchaseRequest(List<ChargeRequest> charge) {
        Map<String, ChargeResponse> responseMap = new HashMap<>();
        try {
            responseMap = references.getChargingHandler().submitChargingRequests(charge);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            responseMap.put(null, new ChargeResponse(ChargingResponseCode.ERROR, "Charging request has not been accepted."));
        }

        return responseMap;
    }

    @Override
    public ChargeResult checkChargingResult(ChargeRequest chargeRequest) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public ChargeTimes getChargingTimes() {
        String[] times = Config.CHARGING_TIMES.split(",");
        List<Date> dates = new ArrayList<>();

        TimeZone timeZone = TimeZone.getTimeZone(Config.CHARGING_TIMES);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        simpleDateFormat.setTimeZone(timeZone);

        for (String time : times) {
            try {
                Date date = simpleDateFormat.parse(time);
                dates.add(date);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return new ChargeTimes(dates, timeZone);
    }

    @Override
    public MessageResponse sendMessage(MessageRequest messageRequest, MessagePriority messagePriority, String s) {
        try {
            references.getMessageHandler().submitMessage(messageRequest, messagePriority, s);
            return new MessageResponse(MessageResponseCode.OK, "Message has been accepted");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return new MessageResponse(MessageResponseCode.ERROR, "Message has not been accepted");
    }

    @Override
    public MSISDNDetailResponse getMSISDNDetails(MSISDNDetailRequest request) {
        return null;
    }

    @Override
    public SubscriptionResponse subscribe(SubscriptionRequest subscriptionRequest, String s) throws SubscriptionException {
        return null;
    }

    @Override
    public SubscriptionStatus directSubscribe(SubscriptionRequest sr, String srh) throws SubscriptionException {
        return null;
    }

    @Override
    public SubscriptionResponse unsubscribe(UnsubscriptionRequest unsubscriptionRequest, String s) throws SubscriptionException {
        return null;
    }

    @Override
    public SubscriptionResult checkSubscriptionStatus(CheckSubscriptionRequest checkSubscriptionRequest) throws SubscriptionException {
        return null;
    }

    @Override
    public SubscriptionStatus verifyPinAndSubscribe(VerifyPinRequest verifyPinRequest, String s) throws SubscriptionException {
        return null;
    }

    @Override
    public MakePinResponse makePinRequest(PinRequest pinRequest) throws SubscriptionException {
        return null;
    }

    @Override
    public VerifyPinResponse verifyPin(VerifyPinRequest verifyPinRequest) throws SubscriptionException {
        return null;
    }


}
