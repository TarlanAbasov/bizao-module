package com.voida.bizao.exception;

/**
Created by: Tarlan Abasov
on 27/02/2019 13:30
*/

public class HttpClientException extends Exception{
    public HttpClientException(){
        super();
    }

    public HttpClientException(String message){
        super(message);
    }

    public HttpClientException(String message, Throwable cause){
        super(message, cause);
    }
}
