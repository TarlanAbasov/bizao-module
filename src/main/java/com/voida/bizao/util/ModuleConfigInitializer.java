package com.voida.bizao.util;

import com.voida.bizao.handler.HttpRequestHandler;
import com.voida.util.ModuleConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:21 PM
 */

public class ModuleConfigInitializer {
    private static final Logger logger = LogManager.getLogger(ModuleConfigInitializer.class);

    public static ModuleConfig initialize(){
        logger.debug("Initializing Bakcell Module configs");

        ModuleConfig config=new ModuleConfig();

        config.setName("Bakcell");

        config.setHttpServerPort(Config.HTTP_SERVER_PORT);
        config.setHttpServerPackage(HttpRequestHandler.class.getPackage().toString());
        config.setParsApiEnabled(Config.PARS_API_ENABLED);
        config.setHttpServerMaxThread(Config.HTTP_SERVER_MAX_THREAD);
        config.setHttpServerMinThread(Config.HTTP_SERVER_MIN_THREAD);
        config.setHttpServerIdleTimeout(Config.HTTP_SERVER_IDLE_TIMEOUT);

        config.setChargingTimeZone(Config.CHARGING_TIMEZONE);
        config.setChargingTimes(Config.CHARGING_TIMES);

        config.setParsApiEnabled(Config.PARS_API_ENABLED);

        config.setTimeloggerThreshold(Config.TIMELOGGER_THRESHOLD);

        config.setDbBatchSize(Config.DB_BATCH_SIZE);
        config.setDbBatchDelay(Config.DB_BATCH_DELAY);

        config.setTpsCharging(Config.TPS_CHARGING);
        config.setTpsMessaging(Config.TPS_MESSAGING);
        config.setMessagingWorkingHours(Config.MESSAGING_WORKING_HOURS);

        config.setWorkerSizeCharging(Config.WORKER_SIZE_CHARGING);
        config.setWorkerSizeMessaging(Config.WORKER_SIZE_MESSAGING);

        config.setJdbcUrl(Config.JDBC_URL);
        config.setJdbcDriver(Config.JDBC_DRIVER);
        config.setJdbcUser(Config.JDBC_USER);
        config.setJdbcPassword(Config.JDBC_PASS);
        config.setJdbcMaxPoolSize(Config.MAX_POOL_SIZE);
        config.setJdbcConnMaxAge(Config.CONN_MAX_AGE        );
        config.setJdbcAutoCommitOnClose(Config.AUTO_COMMIT_ON_CLOSE);
        config.setJdbcConnTestPeriod(Config.CONN_TEST_PERIOD);

        config.setHttpRequestTimeout(Config.HTTP_REQUEST_TIMEOUT);

        logger.debug(config.addName(" Config initialized : "+config));

        return config;
    }
}
