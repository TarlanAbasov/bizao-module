package com.voida.bizao.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:21 PM
 */

public class Config {
    private static Properties properties;
    private static Logger logger = LogManager.getLogger(Config.class);

    public static final String JDBC_DRIVER = get("jdbc.Driver");
    public static final String JDBC_URL = get("jdbc.URL");
    public static final String JDBC_USER = get("jdbc.UserName");
    public static final String JDBC_PASS = get("jdbc.Password");

    public static final int MAX_POOL_SIZE = Integer.valueOf(get("jdbc.MaxPoolSize"));
    public static final int CONN_MAX_AGE = Integer.valueOf(get("jdbc.MaxConnectionAge"));
    public static final int CONN_TEST_PERIOD = Integer.valueOf(get("jdbc.ConnectionTestPeriod"));
    public static final boolean AUTO_COMMIT_ON_CLOSE = get("jdbc.TestConnectionOnCheckin")
        .toUpperCase().equals("YES");

    public static final int HTTP_REQUEST_TIMEOUT = Integer.valueOf(get("httpRequestTimeout"));
    public static final int HTTP_SERVER_PORT = Integer.valueOf(get("http.server.port"));

    public static final float TIMELOGGER_THRESHOLD = Float.valueOf(get("timelogger.threshold"));

    public static final String OPERATOR_NAME = get("OperatorName");
    public static final String CHARGING_TIMES = get("Charging.Times");
    public static final String CHARGING_TIMEZONE = get("Charging.Timezone");

    public static final String MESSAGING_WORKING_HOURS = get("MessagingWorkingHours");

    public static final String PROTOCOL = get("protocol");
    public static final String HOST = get("host");

    public static final int PORT = Integer.valueOf(get("port"));

    public static final String CHARGE_PROTOCOL = get("charge.protocol");
    public static final String CHARGE_HOST = get("charge.host");
    public static final int CHARGE_PORT = Integer.valueOf(get("charge.port"));

    public static final String SEND_MT_PATH = get("send.mt.path");
    public static final String CHARGE_PATH = get("charge.path");

    public static final String AUTH = get("auth");
    public static final String PASSW = get("passw");

    public static final int WORKER_SIZE_MESSAGING = Integer.valueOf(get("workerSize.messaging"));
    public static final int TPS_MESSAGING = Integer.valueOf(get("worker.tps.messaging"));
    public static final int WORKER_SIZE_CHARGING = Integer.valueOf(get("workerSize.charging"));
    public static final int TPS_CHARGING = Integer.valueOf(get("worker.tps.charging"));

    public static final int DB_BATCH_SIZE = Integer.valueOf(get("db.batchSize"));
    public static final int DB_BATCH_DELAY = Integer.valueOf(get("db.batchDelaySeconds"));

    public static boolean PARS_API_ENABLED=Boolean.valueOf(get("par.api.enabled"));
    public static int HTTP_SERVER_MAX_THREAD=Integer.valueOf(get("http.server.max.thread"));
    public static int HTTP_SERVER_MIN_THREAD=Integer.valueOf(get("http.server.min.thread"));
    public static int HTTP_SERVER_IDLE_TIMEOUT=Integer.valueOf(get("http.server.idle.timeout"));



    public static String get(String keyword) {
        if (properties == null) {
            properties = getProperties();
            if (properties == null) {
                logger.info("null properties");
                return null;
            }
        }
        return properties.getProperty(keyword);
    }

    private static Properties getProperties() {
        InputStream fis = null;
        String confFile = "conf" + System.getProperty("file.separator") + "bizao.properties";
        logger.info(confFile);
        try {
            fis = new FileInputStream(confFile);

            Properties properties = new Properties();
            properties.load(fis);
            return properties;
        } catch (IOException ex) {
            try {
                fis = Config.class.getClassLoader().getResourceAsStream(confFile);
                Properties properties = new Properties();
                properties.load(fis);
                return properties;
            } catch (IOException ex1) {
                return null;
            }
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {

            }
        }
    }
}
