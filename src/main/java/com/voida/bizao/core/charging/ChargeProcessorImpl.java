package com.voida.bizao.core.charging;

import com.voida.charging.process.ChargeProcessor;
import com.voida.charging.request.ChargeRequest;
import com.voida.charging.result.ChargeResult;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:17 PM
 */

public class ChargeProcessorImpl implements ChargeProcessor {
    @Override
    public void processCharging(ChargeRequest chargeRequest) {

    }

    @Override
    public ChargeResult processImmediateCharging(ChargeRequest chargeRequest) throws Exception {
        return null;
    }
}
