package com.voida.bizao.core.subscription;

import com.voida.subscription.process.SubscriptionProcessor;
import com.voida.subscription.request.SubscriptionRequest;
import com.voida.subscription.request.UnsubscriptionRequest;
import com.voida.subscription.request.response.SubscriptionResponse;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:49 PM
 */

public class SubscriptionProcessorImpl implements SubscriptionProcessor {
    @Override
    public SubscriptionResponse subscribe(SubscriptionRequest subscriptionRequest, String s) throws Exception {
        return null;
    }

    @Override
    public SubscriptionResponse unsubscribe(UnsubscriptionRequest unsubscriptionRequest, String s) throws Exception {
        return null;
    }
}
