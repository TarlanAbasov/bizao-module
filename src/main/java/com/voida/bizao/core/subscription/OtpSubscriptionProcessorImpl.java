package com.voida.bizao.core.subscription;

import com.voida.pin.request.PinRequest;
import com.voida.pin.request.VerifyPinRequest;
import com.voida.pin.request.response.MakePinResponse;
import com.voida.pin.request.response.VerifyPinResponse;
import com.voida.subscription.process.OtpSubscriptionProcessor;
import com.voida.subscription.result.SubscriptionStatus;

/**
 * @author Tarlan Abasov
 * @datetime 2/27/19 10:50 PM
 */

public class OtpSubscriptionProcessorImpl implements OtpSubscriptionProcessor {
    @Override
    public MakePinResponse makePinRequest(PinRequest pinRequest) throws Exception {
        return null;
    }

    @Override
    public SubscriptionStatus verifyPinAndSubscribe(VerifyPinRequest verifyPinRequest, String s) throws Exception {
        return null;
    }

    @Override
    public VerifyPinResponse verifyPin(VerifyPinRequest verifyPinRequest) throws Exception {
        return null;
    }
}
